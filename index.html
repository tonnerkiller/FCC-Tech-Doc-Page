<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset="UTF-8">
    <title>Technical Documentation Page</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css"></head>
<body>
  <nav id='navbar' onclick='showMenu()'>
    <div id='nav-content'>
      <header id='nav-header'>
      <!-- description of what site is for -->
        <h1>Some sorting algorithms <i class="fas fa-angle-down dropdown-button"></i></h1>
        <p class='hh-hide'>This Tech-Doc example site lists some sorting algorithms. The info is taken from the <a href="https://en.wikipedia.org/wiki/Sorting_algorithm">wikipedia entry for sorting algorithms</a> and the entries for the  respective algorithms.</p>
      </header>
      <section id='nav-links' class='hh-hide'>
        <a href='#Bogosort' class='nav-link'>Bogosort</a>
        <a href='#Samplesort' class='nav-link'>Samplesort</a>
        <a href='#Bucket_sort' class='nav-link'>Bucket sort</a>
        <a href='#Library_sort' class='nav-link'>Library sort</a>
        <a href='#Bubble_sort' class='nav-link'>Bubble sort</a>
      </section>
    </div>
  </nav>
  <main id="main-doc">
    <section class='main-section' id='Bogosort'>
      <header>
        <h2>Bogosort</h2>
      </header>
      <p>Bogosort is a highly ineffective  sorting function based on the generate and test paradigm. The function successively generates permutations of its input until it finds one that is sorted. It is not useful for sorting, but may be used for educational purposes, to contrast it with more efficient algorithms.</p>
      <p>Two versions of the function exist: a deterministic version that enumerates all permutations until it hits a sorted one, and a randomized version that randomly permutes its input. An analogy for the working of the latter version is to sort a deck of cards by throwing the deck into the air, picking the cards up at random, and repeating the process until the deck is sorted. Its name comes from the word bogus. The following is a description of the randomized function in pseudocode:</p>
      <pre class='code'>
        <code>
while not isInOrder(deck):
  shuffle(deck)
        </code>
      </pre>
      <p>Alternative names of the algorithm are:</p>
      <ul>
        <li>permutation sort</li>
        <li>stupid sort</li>
        <li>slow sort</li>
        <li>shotgun sort</li>
        <li>monkey sort</li>
      </ul>
    </section>
    <section class='main-section' id='Samplesort'>
      <header>
        <h2>Samplesort</h2>
      </header>
      <p>Samplesort is a sorting algorithm that is a divide and conquer algorithm often used in parallel processing systems. Conventional divide and conquer sorting algorithms partitions the array into sub-intervals or buckets. The buckets are then sorted individually and then concatenated together. However, if the array is non-uniformly distributed, the performance of these sorting algorithms can be significantly throttled. Samplesort addresses this issue by selecting a sample of size s from the n-element sequence, and determining the range of the buckets by sorting the sample and choosing m -1 elements from the result. These elements (called splitters) then divide the sample into m equal-sized buckets. Samplesort is described in the 1970 paper, "Samplesort: A Sampling Approach to Minimal Storage Tree Sorting", by W. D. Frazer and A. C. McKellar.</p>
      <pre class='code'>
        <code>
sampleSort(A[1..n], k, p)
  if n/k < threshold then smallSort(A) // if bucket size is below a threshold switch to e.g. quicksort
  select S = [S<sub>1</sub>,..., S<sub>p(k-1)</sub>] randomly from A // select samples
  sort S // sort samples
  [s<sub>0</sub>, s<sub>1</sub>, s<sub>p-1</sub>, s<sub>p</sub>] &larr; [-&infin;, S<sub>k</sub>, S<sub>2k</sub>,..., S<sub>(p-1)k</sub>, &infin;] // select splitters
  //clasify elements
  for each a &isin; A
    find j such that s<sub>j-1</sub> < a &le; s<sub>j</sub>
    place a in bucket b<sub>j</sub>
    return concatenate(sampleSort(b<sub>1</sub>),..., sampleSort(b<sub>k</sub>))
        </code>
      </pre>
      <p>The pseudo code is different from the original Frazer and McKellar algorithm. In the pseudo code, samplesort is called recursively. Frazer and McKellar called samplesort just once and used quicksort in all following iterations.</p>
    </section>
    <section class='main-section' id='Bucket_sort'>
      <header>
        <h2>Bucket sort</h2>
      </header>
      <p>Bucket sort, or bin sort, is a sorting algorithm that works by distributing the elements of an array into a number of buckets. Each bucket is then sorted individually, either using a different sorting algorithm, or by recursively applying the bucket sorting algorithm. It is a distribution sort, a generalization of pigeonhole sort, and is a cousin of radix sort in the most-to-least significant digit flavor. Bucket sort can be implemented with comparisons and therefore can also be considered a comparison sort algorithm. The computational complexity estimates involve the number of buckets.</p>
      <pre class='code'>
        <code>
function bucketSort(array, n) is
  buckets ← new array of n empty lists
  for i = 0 to (length(array)-1) do
    insert array[i] into buckets[msbits(array[i], k)]
  for i = 0 to n - 1 do
    nextSort(buckets[i]);
  return the concatenation of buckets[0], ...., buckets[n-1]
        </code>
      </pre>
      <p>A common optimization is to put the unsorted elements of the buckets back in the original array first, then run insertion sort over the complete array; because insertion sort's runtime is based on how far each element is from its final position, the number of comparisons remains relatively small, and the memory hierarchy is better exploited by storing the list contiguously in memory.[</p>
    </section>
    <section class='main-section' id='Library_sort'>
      <header>
        <h2>Library sort</h2>
      </header>
      <p>Library sort, or gapped insertion sort is a sorting algorithm that uses an insertion sort, but with gaps in the array to accelerate subsequent insertions. The name comes from an analogy:</p>
      <blockquote>Suppose a librarian were to store his books alphabetically on a long shelf, starting with the As at the left end, and continuing to the right along the shelf with no spaces between the books until the end of the Zs. If the librarian acquired a new book that belongs to the B section, once he finds the correct space in the B section, he will have to move every book over, from the middle of the Bs all the way down to the Zs in order to make room for the new book. This is an insertion sort. However, if he were to leave a space after every letter, as long as there was still space after B, he would only have to move a few books to make room for the new one. This is the basic principle of the Library Sort.</blockquote>
      <p>Let us say we have an array of n elements. We choose the gap we intend to give. Then we would have a final array of size (1 + ε)n. The algorithm works in log n rounds. In each round we insert as many elements as there are in the final array already, before re-balancing the array. For finding the position of inserting, we apply Binary Search in the final array and then swap the following elements till we hit an empty space. Once the round is over, we re-balance the final array by inserting spaces between each element.</p>
      <p>Following are three important steps of the algorithm:</p>
      <ol>
        <li>Binary Search: Finding the position of insertion by applying binary search within the already inserted elements. This can be done by linearly moving towards left or right side of the array if you hit an empty space in the middle element.</li>
        <li> Insertion: Inserting the element in the position found and swapping the following elements by 1 position till an empty space is hit.</li>
        <li>Re-Balancing: Inserting spaces between each pair of elements in the array. This takes linear time, and because there are log n rounds in the algorithm, total re-balancing takes O(n log n) time only.</li>
      </ol>
      <pre class='code'>
        <code>
proc rebalance(A, begin, end)
  r ← end
  w ← end * 2
  while r >= begin
    A[w+1] ← gap
    A[w] ← A[r]
    r ← r - 1
    w ← w - 2
        </code>
      </pre>
      <pre class='code'>
        <code>
proc sort(A)
  n ← length(A)
  S ← new array of n gaps
  for i ← 1 to floor(log2(n) + 1)
    for j ← 2^i to 2^(i+1)
      ins ← binarysearch(A[j], S, 2^(i-1))
        insert A[j] at S[ins]
        </code>
      </pre>
    </section>
    <section class='main-section' id='Bubble_sort'>
      <header>
        <h2>Bubble sort</h2>
      </header>
      <p>Bubble sort, sometimes referred to as sinking sort, is a simple sorting algorithm that repeatedly steps through the list to be sorted, compares each pair of adjacent items and swaps them if they are in the wrong order. The pass through the list is repeated until no swaps are needed, which indicates that the list is sorted. The algorithm, which is a comparison sort, is named for the way smaller or larger elements "bubble" to the top of the list. Although the algorithm is simple, it is too slow and impractical for most problems even when compared to insertion sort. Bubble sort can be practical if the input is in mostly sorted order with some out-of-order elements nearly in position.
      <pre class='code'>
        <code>
procedure bubbleSort( A : list of sortable items )
  n = length(A)
  repeat
    swapped = false
    for i = 1 to n-1 inclusive do
      /* if this pair is out of order */
      if A[i-1] > A[i] then
        /* swap them and remember something changed */
        swap( A[i-1], A[i] )
        swapped = true
      end if
    end for
  until not swapped
end procedure
        </code>
      </pre>
      <p>Although bubble sort is one of the simplest sorting algorithms to understand and implement, its O(n2) complexity means that its efficiency decreases dramatically on lists of more than a small number of elements. Even among simple O(n2) sorting algorithms, algorithms like insertion sort are usually considerably more efficient.</p>
    </section>
  </main>
  <script
			  src="https://code.jquery.com/jquery-3.3.1.js"
			  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
			  crossorigin="anonymous"></script>
  <script src="https://cdn.freecodecamp.org/testable-projects-fcc/v1/bundle.js"></script>
  <script src="js/index.js"></script>
</body>
</html>
